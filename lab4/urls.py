from django.contrib import admin
from django.urls import path

from . import views

app_name = 'lab4'
urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('about/', views.profil, name='about'),
    path('register/', views.register, name='register'),
    path('list/', views.list_jadwal, name='list_jadwal'),
    path('create/', views.create_jadwal, name='create_jadwal'),
    path('delete/all/', views.delete_jadwal_all, name='delete_jadwal_all'),
    
]