from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import JadwalPribadi
from .forms import Jadwal_Form


# Create your views here.
def homepage(request):
	return render(request,'home.html')

def profil(request):
	return render(request,'about-me.html')

def register(request):
	return render(request,'kontak.html')

def create_jadwal(request):
	form = Jadwal_Form(request.POST or None)
	response = {}
	if(request.method == "POST"):
	    if (form.is_valid()):
	        hari = request.POST.get("hari")
	        tanggal = request.POST.get("tanggal")
	        jam = request.POST.get("jam")
	        kategori = request.POST.get("kategori")
	        nama_kegiatan = request.POST.get("nama_kegiatan")
	        tempat = request.POST.get("tempat")

	        JadwalPribadi.objects.create(hari=hari, tanggal=tanggal, jam=jam, kategori=kategori, nama_kegiatan=nama_kegiatan, tempat=tempat)
	        response = {
	            "message" : "Jadwal Created",
	            "color" : "green",
	        }
	        return redirect('lab4:list_jadwal')
	    else:
	    	response = {
	    		"form" : Jadwal_Form,
	    		"message" : "Form is not Valid",
	    		"color" : "red",
	    	}
	    	return render(request, 'create_jadwal.html', response)

	response['form'] = form
	return render(request,'create_jadwal.html', response)

def list_jadwal(request):

	schedules = JadwalPribadi.objects.all()
	response = {
	    "jadwals" : schedules
	}
	return render(request, 'list_jadwal.html', response)

def delete_jadwal_all(request):
    jadwal = JadwalPribadi.objects.all().delete()
    return redirect('lab4:list_jadwal')
